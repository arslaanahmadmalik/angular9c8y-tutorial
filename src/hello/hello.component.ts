import { _, DashboardChildDimension } from '@c8y/ngx-components';
import { Component } from '@angular/core';

/**
 * The hello.component shows a short introduction text and
 * a little list of things that you can discover within this
 * tutorial application.
 */
@Component({
  selector: 'hello',
  templateUrl: './hello.component.html'
})
export class HelloComponent {
  introductionText: string;
  featureList: string[];
  widgets = [
    { x: 0, y: 0, width: 12, height: 1 },
    { x: 3, y: 1, width: 5, height: 2 }
  ] as DashboardChildDimension[];
  isFrozen = false;
  showTitle = true;

  constructor() {
    // _ annotation to mark this string as translatable string.
    this.introductionText = _(
      '... this basic Application will show you some concepts and components about the:'
    );
    this.featureList = [
      _('Navigator (panel on the left)'),
      _('Tabs (within World-page)'),
      _('Breadcrumbs (below the c8y-title)'),
      _('Global actions (top right corner (+)-Button)'),
      _('Local actions (within Superhero-page)'),
      _('Fetching data (within Device-page)'),
      _('Dashboards')
    ];
  }

  /**
   * Add a random child to the dashboard.
   */
  addRandom() {
    let width = Math.round(Math.random() * 10);
    if (width < 2) {
      width += 2;
    }

    let height = Math.round(Math.random() * 10);
    if (height < 1) {
      height += 1;
    }
    this.widgets.push({ width, height });
  }

  /**
   * Called when the user change the dashboard
   */
  dashboardChange(event) {
    console.log(event);
  }
}
